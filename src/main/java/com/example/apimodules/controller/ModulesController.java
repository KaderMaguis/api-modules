package com.example.apimodules.controller;
package com.example.apimodules.service;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController

public class ModulesController {
    @GetMapping("/modules")
    public String modules(){
        ModuleService service = new ModuleService;
        return service.getModules();
    }
}
